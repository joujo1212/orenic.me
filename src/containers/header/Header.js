import React, {Component} from 'react';
import css from './styles.css';
import Menu from 'components/menu/Menu';
import Social from 'components/social/Social';

class Header extends Component {
  render() {
    return (
      <div className={css.wrapper}>
        <div className={css.container}>
          <Menu />
          <h1 className={css.name}>Jozef Orenič</h1>
          <h2 className={css.subtitle}>Frontend developer & HW lover</h2>
          <Social />
        </div>
      </div>
    );
  }
}

export default Header;
