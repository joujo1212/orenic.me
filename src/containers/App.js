import React, { Component } from 'react';
import Header from './header/Header';
import Skills from './skills/Skills';
import {maintenance} from 'pageConfig';
import Maintenance from 'components/maintenance/Maintenance';
import MetaTags from 'react-meta-tags';
import {getLang} from '../pageConfig';

class App extends Component {
  render() {
    return (
      <div>
        <MetaTags>
          <title>{getLang().title}</title>
          <meta id="description" name="description" content={getLang().description} />
          <meta id="author" name="author" content={getLang().author} />
          <meta id="keywords" name="keywords" content={getLang().keywords} />
          <meta id="og-title" property="og:title" content={getLang().title} />
          <meta id="og-description" property="og:description" content={getLang().description} />
        </MetaTags>
        {maintenance === false
          ? <div className="App">
              <Header/>
              <Skills />
            </div>
          : <Maintenance />}
      </div>
    );
  }
}

export default App;
