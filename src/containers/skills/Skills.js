import React, {Component} from 'react';
import Skill from '../../components/skill/Skill';
import css from './styles.css';
import DynamicVisibility from '../../components/dynamicVisibility/DynamicVisibility';
import {skills} from '../../pageConfig';

class Skills extends Component {
  render() {
    return (
      <div className={css.wrapper}>
        <DynamicVisibility>
          {skills.map((skill, i) =>
            <Skill key={i} name={skill.name} level={skill.level}/>
          )}
        </DynamicVisibility>
      </div>
    );
  }
}

export default Skills;
