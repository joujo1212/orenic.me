export const skills = [
  {name: 'HTML', level: 90},
  {name: 'CSS3', level: 60},
  {name: 'Javascript', level: 80},
  {name: 'Java EE', level: 50},
];

export const imagesPath = 'img/';

export const socials = [
  {image: 'LinkedIN_White.svg', hoverImage: 'LinkedIN_Color.svg'},
  {image: 'Youtube_White.svg', hoverImage: 'Youtube_Color.svg'},
  {image: 'Facebook_White.svg', hoverImage: 'Facebook_Color.svg'},
  {image: 'GooglePlus_White.svg', hoverImage: 'GooglePlus_Color.svg'},
];

export const menuItems = [
  {label: 'About Me', url: '#about-me'},
  {label: 'My Skills', url: '#my-skills'},
  {label: 'My Hobbies', url: '#my-hobbies'},
  {label: 'My Works', url: '#my-works'},
  {label: 'Contact Me', url: '#contact-me'},
];

export const maintenance = true;


export const getLang = () => {
  if (navigator.language.toLowerCase().indexOf('sk') !== -1 ) {
    return {
      title: 'Osobné stránky Jozefa Oreniča',
      author: 'Jozef Orenič',
      description: 'Osobné stránky Jozefa Oreniča',
      keywords: 'jozef orenič, orenič, webdesign, hardware, frontend developer, web developer',
      maintenance: {
        caption: 'Na stránke usilovne pracujem. Naozaj.',
        but: 'avšak',
        visitBlog: 'môžete navštíviť môj blog',
        coffee: 'Dajte si kávu a skúste to neskôr.'
      }
    }
  } else {
    return {
      title: 'Jozef Orenic\'s personal pages',
      author: 'Jozef Orenic',
      description: 'Jozef Orenic\'s personal pages',
      keywords: 'jozef orenic, orenic, webdesign, hardware, frontend developer, web developer',
      maintenance: {
        caption: 'I\'m working on page really, really hard right now.',
        but: 'but',
        visitBlog: 'you can visit my blog',
        coffee: 'Take a cup of coffee and try it later.'
      }
    }
  }
};