import React, {Component, PropTypes} from 'react';
import css from './styles.css';
import {Line} from 'rc-progress';

class Skill extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    level: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      level: 0,
      levelLabel: 0,
      levelTimer: null
    };
  }

  timerCallback() {
    if (this.state.levelLabel === this.props.level) {
      clearTimeout(this.state.levelTimer);
    } else {
      this.setState({levelLabel: ++this.state.levelLabel})
    }
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({level: this.props.level});
      this.setState({
        levelTimer: setInterval(this.timerCallback.bind(this), 7)
      });
    }, 2000);
  }

  render() {
    return (
      <div className={css.wrapper}>
        <h2 className={css.name}>{this.props.name}</h2>
        <div className={css.liner}>
          <Line className={css.bar} percent={this.state.level} strokeWidth="2" strokeColor="#55ec91" trailColor="#f1f1f1" trailWidth="100"/>
          <div className={css.level}>{this.state.levelLabel}</div>
        </div>
      </div>
    );
  }
}

export default Skill;
