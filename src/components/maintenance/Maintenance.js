import React, {PropTypes} from 'react';
import css from './styles.css';
import me from './me.jpg';
import b1 from './bubble1.jpg';
import b2 from './bubble2.jpg';
import b3 from './bubble3.jpg';
import b4 from './bubble4.jpg';
import {getLang} from '../../pageConfig';

const Maintenance = () => {
  return (
    <div className={css.wrapper}>
      <div className={css.text}>{getLang().maintenance.caption}</div>
      <div className={css.textSmall + ' ' + css.bubblesHandler}>
        <span>{getLang().maintenance.but}</span>
        <a href="http://blog.orenic.me/?utm_source=orenic.me&utm_medium=own_link" className={css.link}>
          {getLang().maintenance.visitBlog}
          <img className={css.image} src={me} />
        </a>
        <div className={css.bubbles}>
          <img src={b1} />
          <img src={b2} />
          <img src={b3} />
          <img src={b4} />
        </div>
      </div>
      <div className={css.subText}>{getLang().maintenance.coffee}</div>
    </div>
  );
};

export default Maintenance;
