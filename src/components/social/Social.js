import React from 'react';
import css from './styles.css';
import SocialButton from 'components/socialButton/SocialButton';
import {socials} from 'pageConfig';

const Social = () => {
  return (
    <div className={css.wrapper}>
      {socials.map((item, i) =>
        <SocialButton key={i} image={item.image} hoverImage={item.hoverImage} className={css.button} />
      )}
    </div>
  );
};

export default Social;
