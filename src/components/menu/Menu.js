import React from 'react';
import css from './styles.css';
import MenuItem from 'components/menuItem/MenuItem';
import {menuItems} from 'pageConfig';

const Menu = () => {
  return (
    <div className={css.wrapper}>
      {menuItems.map((item, i) =>
        <div key={i} >
          <MenuItem label={item.label} url={item.url} isActive={false} />
          {i + 1 < menuItems.length ? <div className={css.separator} /> : '' }
        </div>
      )}
      <div className={css.separator}>|</div>
      <MenuItem label='Blog.orenic.me' url={'http://blog.orenic.me'} isActive={false} className={css.externalLink}/>
    </div>
  );
};

export default Menu;
