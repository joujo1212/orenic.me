import React, {Component, PropTypes} from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import css from './styles.css';

class DynamicVisibility extends Component {
  static propTypes = {
    children: PropTypes.any.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      visibleSkills: false
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(isVisible) {
    this.setState({visibleSkills: isVisible});
  };

  render() {
    return (
      <VisibilitySensor onChange={this.onChange} partialVisibility={true} delay={100}>
        <div className={this.state.visibleSkills === false ? css.hide : css.show}>{this.props.children}</div>
      </VisibilitySensor>
    );
  }
}

export default DynamicVisibility;
