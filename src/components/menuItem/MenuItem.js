import React, {PropTypes} from 'react';
import css from './styles.css';

const MenuItem = ({label, url, isActive, className}) => {
  return (
    <a href={url} className={css.wrapper + ' ' + className}>
      <div className={css.label}>{label}</div>
      <div className={css.line + (isActive === true ? ' ' + css.active : '')} />
    </a>
  );
};

MenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  isActive: PropTypes.bool.isRequired,
  className: PropTypes.string
};

export default MenuItem;
