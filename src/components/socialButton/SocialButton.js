import React, {PropTypes} from 'react';
import css from './styles.css';
import {imagesPath} from 'pageConfig';

const SocialButton = ({image, hoverImage, className}) => {
  return (
    <div className={css.wrapper + ' ' + className}>
      <img src={imagesPath + image} className={css.image} />
      <img src={imagesPath + hoverImage} className={css.hoverImage} />
    </div>
  )
};

SocialButton.propTypes = {

};

export default SocialButton;